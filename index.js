function createNewUser() {
    let newUser = {
        _firstName: prompt('Введите имя'),
        get firstName() {
            return this._firstName;
        },
        set firstName(value) {
            this._firstName = value;
        },
        _lastName: prompt('Введите фамилию'),
        get lastName() {
            return this._lastName;
        },
        set lastName(value) {
            this._lastName = value;
        },
        getLogin: () => (newUser.firstName.charAt(0) + newUser.lastName).toLowerCase(),
    }
    return newUser;
}

let user = createNewUser();
alert(`Ваш логин: ${user.getLogin()}`);

let a = confirm('Хотите изменить имя и фамилию?')
if (a === true) {
    user.firstName = prompt('Введите новое имя: ', user.firstName);
    user.lastName = prompt('Введите новую фамилию: ', user.lastName)
    alert(`Ваше новое имя, фамилия и логин: ${user.firstName} ${user.lastName}, ${user.getLogin()}`)
}

// function createNewUser() {
//     let newUser = new Object();
//     newUser.firstName = prompt('Введите имя');
//     newUser.lastName = prompt('Введите фамилию');
//     newUser.getLogin = () => (newUser.firstName.charAt(0) + newUser.lastName).toLowerCase();
//     return newUser;
// }

// let user = createNewUser();
// alert(user.getLogin());